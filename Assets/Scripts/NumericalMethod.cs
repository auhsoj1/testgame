﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumericalMethod : MonoBehaviour {

    public float[] A_ip = { 0, 0, 0 };
    public float[] A_i = { 0, 0, 0 };
    public float[] B_i = { 0, 0, 0};
    public float L_i, theta_i, theta_j = 0;
    public float x, y, z;
    public float s, r = 0;

    private void Start()
    {
        //   A_i = [a1_ip * cos(q2) + a2_ip * sin(q1) * sin(q2) + a3_ip * cos(q1) * sin(q2),...
        //           a2_ip * cos(q1) + -1 * a3_ip * sin(q1),... 
        //           - 1 * a1_ip * sin(q2) + a2_ip * sin(q1) * cos(q2) + a3_ip * cos(q1) * cos(q2)];
        for (int i = 0; i < B_i.Length; i++) {
            L_i = L_i + (float)Math.Pow((A_i[i] - B_i[i]), 2.0);
        }        

        L_i = (float)Math.Sqrt(L_i);
        //Not sure if the following numerical analysis method would work
        //Newton - Raphson Method
        theta_i = 0; // start here
        theta_j = 0; // j = i + 1

        x = (float)Math.Sqrt(Math.Pow(s, 2) - Math.Pow(r, 2) * Math.Sin(theta_i) * Math.Sin(theta_i));
        y = (float)(Math.Pow(r, 2) * Math.Cos(theta_i) * Math.Sin(theta_i) / x);
        z = (float)((L_i - r * Math.Cos(theta_i) - x) / (r * Math.Cos(theta_i) + y));
        theta_j = theta_i - z;
        while (theta_i != theta_j) {
            theta_i = theta_i + 1;
            x = (float)(Math.Sqrt(Math.Pow(s, 2) - Math.Pow(r, 2) * Math.Sin(theta_i) * Math.Sin(theta_i)));
            y = (float)(Math.Pow(r, 2) * Math.Cos(theta_i) * Math.Sin(theta_i) / x);
            z = (float)((L_i - r * Math.Cos(theta_i) - x) / (r * Math.Cos(theta_i) + y));
            theta_j = theta_i - z;
        }

    }

}

using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System;

//Code of plugin class

public class skeletoncode : MonoBehaviour
{

    private float currentp; //current pitch
    private float currentr; //current roll
    private bool in_motion;
    private float MAX_ROTATION = 7.5f; //degrees
    private float MAX_DPS = 210; //degrees per second
    private float MAX_SYMBOLIC_ROTATION = 100; //percentage
    private float INTENSITY1; // low motor speed
    private float INTENSITY2; // medium motor speed
    private float INTENSITY3; // high motor speed
    private float pholder; //variable to pass data to update. pitch delta
    private float rholder; //variable to pass data to update. roll delta
    private int iholder; //variable to pass data to update. intensity
    private bool readable; //variable to pass data to update
    private bool finishedTilting; //has processor finished current tilt operation

    float[] A_1p = { 11f, 11f, 0f };
    float[] A_2p = { -11f, 11f, 0f };
    float[] B_1 = { 11f, 11f, -10.25f };
    float[] B_2 = { -11f, 11f, -10.25f };
    float theta1, theta2;
    float[] theta; //values range from 0 to pi
    float[] L;

    GameObject player;
    private SerialPort serialPort;

    float startTime;
    float currentTime;
    bool startGame = false;
    void Start()
    {
        player = GameObject.Find("Player");
        string[] names = SerialPort.GetPortNames();
        //for (int i = 0; i < names.Length; i++)
        //{
        serialPort = new SerialPort("COM8", 115200, Parity.None, 8, StopBits.One);
        try
        {
            serialPort.Open();
            serialPort.ReadTimeout = 1;
            if (serialPort.IsOpen)
            {
                Debug.Log("yay");
            }
        }
        catch (IOException ex)
        {
            Debug.Log("not this one");
        }
        //}

        initIVRA();
        
    }

    //The main workhorse of the Plugin
    void Update()
    {
        //receiveMessage();
        if (readable)
        {
            if (!in_motion)
            {
                float deltap = pholder;
                float deltar = rholder;
                int intensity = iholder;
                tilt(deltap, deltar, intensity);
            }
            readable = false;
        }
        //1
        if (Input.GetKey("p"))
        {
            startGame = true;
            startTime = Time.time;
        }
        if (startGame)
        {
            if ((Time.time - startTime > 0.75f) && (Time.time - startTime < 0.8f))
            {
                tilt(-7.5f, 0, 3);
            }
            //2
            if ((Time.time - startTime > 9.0f) && (Time.time - startTime < 9.1f))
            {
                tilt(0, -7.5f, 3);
            }
            //3
            if ((Time.time - startTime > 11.0f) && (Time.time - startTime < 11.1f))
            {
                tilt(-7.5f, -7.5f, 3);
            }
            //4
            if ((Time.time - startTime > 11.5f) && (Time.time - startTime < 11.6f))
            {
                tilt(-7.5f, 0, 3);
            }
            //5
            if ((Time.time - startTime > 13.5f) && (Time.time - startTime < 13.6f))
            {
                tilt(-7.5f, 4.0f, 3);
            }
            //6
            if ((Time.time - startTime > 14.5f) && (Time.time - startTime < 14.6f))
            {
                tilt(-7.5f, 7.5f, 3);
            }
            //7
            if ((Time.time - startTime > 15.0f) && (Time.time - startTime < 15.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //8
            if ((Time.time - startTime > 16.0f) && (Time.time - startTime < 16.1f))
            {
                tilt(7.5f, 7.5f, 3);
            }
            //9
            if ((Time.time - startTime > 17.0f) && (Time.time - startTime < 17.1f))
            {
                tilt(7.5f, 4.0f, 3);
            }
            //9.5
            if ((Time.time - startTime > 19.0f) && (Time.time - startTime < 19.1f))
            {
                tilt(0, 0, 3);
            }
            //10
            if ((Time.time - startTime > 24.0f) && (Time.time - startTime < 24.1f))
            {
                tilt(7.5f, -7.5f, 3);
            }
            //10.5
            if ((Time.time - startTime > 26.0f) && (Time.time - startTime < 26.1f))
            {
                tilt(0, 0, 3);
            }
            //11
            if ((Time.time - startTime > 28.0f) && (Time.time - startTime < 28.1f))
            {
                tilt(-7.5f, -7.5f, 3);
            }
            //12
            if ((Time.time - startTime > 30.0f) && (Time.time - startTime < 30.1f))
            {
                tilt(-7.5f, 0, 3);
            }
            //13 slight back left
            if ((Time.time - startTime > 33.0f) && (Time.time - startTime < 33.1f))
            {
                tilt(-4.0f, 7.5f, 2);
            }
            //14
            if ((Time.time - startTime > 34.0f) && (Time.time - startTime < 34.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //15
            if ((Time.time - startTime > 34.5f) && (Time.time - startTime < 34.6f))
            {
                tilt(0, 7.5f, 3);
            }
            //15.5
            if ((Time.time - startTime > 39.0f) && (Time.time - startTime < 39.1f))
            {
                tilt(0, 0, 3);
            }
            //16
            if ((Time.time - startTime > 40.0f) && (Time.time - startTime < 40.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //17
            if ((Time.time - startTime > 41.0f) && (Time.time - startTime < 41.1f))
            {
                tilt(-7.5f, 7.5f, 3);
            }
            //18
            if ((Time.time - startTime > 42.0f) && (Time.time - startTime < 42.1f))
            {
                tilt(-7.5f, 0, 2);
            }
            //19
            if ((Time.time - startTime > 44.75f) && (Time.time - startTime < 44.8f))
            {
                tilt(-4.0f, 7.5f, 3);
            }
            //20
            if ((Time.time - startTime > 45.25f) && (Time.time - startTime < 45.3f))
            {
                tilt(-7.5f, 0, 2);
            }
            //20.5
            if ((Time.time - startTime > 48.5f) && (Time.time - startTime < 48.6f))
            {
                tilt(0, 0, 2);
            }
            //21
            if ((Time.time - startTime > 49.5f) && (Time.time - startTime < 49.6f))
            {
                tilt(0, 7.5f, 3);
            }
            //22
            if ((Time.time - startTime > 50.0f) && (Time.time - startTime < 50.1f))
            {
                tilt(7.5f, 7.5f, 3);
            }
            //23
            if ((Time.time - startTime > 52.0f) && (Time.time - startTime < 52.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //24
            if ((Time.time - startTime > 52.5f) && (Time.time - startTime < 52.6f))
            {
                tilt(0, -7.5f, 3);
            }
            //25
            if ((Time.time - startTime > 53.0f) && (Time.time - startTime < 53.1f))
            {
                tilt(-7.5f, -7.5f, 3);
            }
            //26
            if ((Time.time - startTime > 54.25f) && (Time.time - startTime < 54.3f))
            {
                tilt(-7.5f, 0, 3);
            }
            //27 right is neg
            if ((Time.time - startTime > 57.0f) && (Time.time - startTime < 57.1f))
            {
                tilt(0, -7.5f, 3);
            }
            //28
            if ((Time.time - startTime > 57.5f) && (Time.time - startTime < 57.6f))
            {
                tilt(7.5f, -7.5f, 3);
            }
            //28.5
            if ((Time.time - startTime > 59.0f) && (Time.time - startTime < 59.1f))
            {
                tilt(0, 0, 3);
            }
            //29
            if ((Time.time - startTime > 60.0f) && (Time.time - startTime < 60.1f))
            {
                tilt(7.5f, -7.5f, 3);
            }
            //29.5
            if ((Time.time - startTime > 61.5f) && (Time.time - startTime < 61.6f))
            {
                tilt(0, 0, 3);
            }
            //30
            if ((Time.time - startTime > 68.0f) && (Time.time - startTime < 68.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //30.5
            if ((Time.time - startTime > 71.0f) && (Time.time - startTime < 71.1f))
            {
                tilt(0, 0, 3);
            }
            //31
            if ((Time.time - startTime > 73.0f) && (Time.time - startTime < 73.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //31.5
            if ((Time.time - startTime > 74.0f) && (Time.time - startTime < 74.1f))
            {
                tilt(0, 0, 3);
            }
            //32
            if ((Time.time - startTime > 77.0f) && (Time.time - startTime < 77.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //33
            if ((Time.time - startTime > 77.5f) && (Time.time - startTime < 77.6f))
            {
                tilt(-7.5f, 7.5f, 3);
            }
            //34
            if ((Time.time - startTime > 79.0f) && (Time.time - startTime < 79.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //34.5
            if ((Time.time - startTime > 80.25f) && (Time.time - startTime < 80.3f))
            {
                tilt(0, 0, 3);
            }
            //35
            if ((Time.time - startTime > 82.0f) && (Time.time - startTime < 82.1f))
            {
                tilt(0, -7.5f, 2);
            }
            //36
            if ((Time.time - startTime > 85.0f) && (Time.time - startTime < 85.1f))
            {
                tilt(7.5f, -7.5f, 3);
            }
            //36.5
            if ((Time.time - startTime > 87.0f) && (Time.time - startTime < 87.1f))
            {
                tilt(0, 0, 3);
            }
            //37
            if ((Time.time - startTime > 88.5f) && (Time.time - startTime < 88.6f))
            {
                tilt(0, 7.5f, 3);
            }
            //38
            if ((Time.time - startTime > 89.5f) && (Time.time - startTime < 89.6f))
            {
                tilt(7.5f, 7.5f, 3);
            }
            //38.5
            if ((Time.time - startTime > 91.0f) && (Time.time - startTime < 91.1f))
            {
                tilt(0, 0, 3);
            }
            //39
            if ((Time.time - startTime > 92.0f) && (Time.time - startTime < 92.1f))
            {
                tilt(0, 7.5f, 3);
            }
            //40
            if ((Time.time - startTime > 94.0f) && (Time.time - startTime < 94.1f))
            {
                tilt(7.5f, 0, 3);
            }
            //41
            if ((Time.time - startTime > 96.0f) && (Time.time - startTime < 96.1f))
            {
                tilt(0, 0, 3);
            }
        }

        if (Input.GetKey("w"))
        {
            tilt(7.5f, 0, 2);
            Debug.Log("here");
        }
        if (Input.GetKey("s"))
        {
            tilt(-7.5f, 0, 2);
        }
        if (Input.GetKey("a"))
        {
            tilt(0, 7.5f, 2);
        }
        if (Input.GetKey("d"))
        {
            tilt(0, -7.5f, 2);
        }
        if (Input.GetKey("x"))
        {
            tilt(0, 0, 2);
        }
        if (Input.GetKey("q"))
        {
            tilt(7.5f, 7.5f, 2);
        }
        if (Input.GetKey("e"))
        {
            tilt(7.5f, -7.5f, 2);
        }
        if (Input.GetKey("z"))
        {
            tilt(-7.5f, 7.5f, 2);
        }
        if (Input.GetKey("c"))
        {
            tilt(-7.5f, -7.5f, 2);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        /*if (other.gameObject == player)
        {
            float myPitch = 0.1309f;
            float myRoll = 0f;
            tilt(myPitch, myRoll, 2);
        }*/
    }

    private void sendMessage(float p, float r, int intensity)//sends messages to the IVAR
    {
        //concatonate the data in a consistent manner and write it to the serial port
        string toSend = p.ToString() + " " + r.ToString() + " " + intensity.ToString();
        Debug.Log(toSend);
        serialPort.WriteLine(toSend);
    }

    private void receiveMessage()
    {
        //need the processor to also send a bool if it is currently executing a command
        string msg = "";
        try
        {
            msg = serialPort.ReadLine();
            string[] words = msg.Split(' ');
            if (words.Length < 2)
            {
                finishedTilting = Convert.ToBoolean(words[0]);
            }
            else
            {
                currentp = (float)Convert.ToDouble(words[0]);
                currentr = (float)Convert.ToDouble(words[1]);
                in_motion = Convert.ToBoolean(Convert.ToDouble(words[2]));
            }
        }
        catch (TimeoutException) { }
    }

    private void initIVRA()//run first
    {
        currentp = 0.0f;
        currentr = 0.0f;
        readable = false;
        in_motion = false;
        finishedTilting = false;
    }

    private void tilt(float pdelta, float rdelta, int intensity)
    {
        float newp = currentp + pdelta;
        float newr = currentr + rdelta;
        if (newp > MAX_SYMBOLIC_ROTATION)
        {
            newp = MAX_SYMBOLIC_ROTATION;
            //too far back
        }
        else if (newp < -MAX_SYMBOLIC_ROTATION)
        {
            newp = -MAX_SYMBOLIC_ROTATION;
            //too far forward
        }
        if (newr > MAX_SYMBOLIC_ROTATION)
        {
            newr = MAX_SYMBOLIC_ROTATION;
            //too far right
        }
        else if (newr < -MAX_SYMBOLIC_ROTATION)
        {
            newr = -MAX_SYMBOLIC_ROTATION;
            //too far left
        }
        sendMessage(newp, newr, intensity);
    }

    private void tiltNeutral(int intensity)//sets the platform back to level
    {
        pholder = -currentp;
        rholder = -currentr;
        iholder = intensity;
        readable = true;
    }

    private void tiltRightMax(int intensity)
    {
        float newr = MAX_SYMBOLIC_ROTATION - currentr;
        pholder = -currentp;
        rholder = newr;
        iholder = intensity;
        readable = true;
    }

    private void tiltLeftMax(int intensity)
    {
        float newr = currentr - MAX_SYMBOLIC_ROTATION;
        pholder = -currentp;
        rholder = newr;
        iholder = intensity;
        readable = true;
    }

    private void tiltBackwardsMax(int intensity)
    {
        float newp = MAX_SYMBOLIC_ROTATION - currentp;
        pholder = newp;
        rholder = -currentr;
        iholder = intensity;
        readable = true;
    }

    private void tiltForwardMax(int intensity)
    {
        float newp = currentp - MAX_SYMBOLIC_ROTATION;
        pholder = newp;
        rholder = -currentr;
        iholder = intensity;
        readable = true;
    }


    //These will be the public use cases that developers have access to

    /*
    Tilt is the primitive that all other use cases stem from. Use this to create unique effects.
    INPUTS:
    @@@@ p: Float values between -100 and 100 that indicates the percentage the platforms pitch should change. -values tilt forward, +values tilt backwards 
    @@@@ r: Float values between -100 and 100 that indicates the percentage the platforms roll should change.  -values tilt left, +values tilt right
    @@@@ intensity: value of 1, 2, or 3 for low, medium, and high intensity motion. This affects how quickly the platform moves and executes the effect

    $$description of what the function does$$
    */
    public void TILT(float p, float r, float intensity)
    {

    }

    /*
    Full body rumble is a function to simulate a shaking motion similar to the rubble on a controller. Use this when the very ground the player is on is shaking. 
    INPUTS:
    @@@@ intensity: value of 1, 2, or 3 for low, medium, and high intensity motion. This affects how quickly the platform moves and executes the effect
    @@@@ duration: Value in seconds that determines how long the effect will exist before it is stopped

    $$description of what the function does$$
    */
    public void FULL_BODY_RUMBLE(float intensity, float duration)
    {

    }

    /*
    Accelerate forwards is a function to simulation the feeling of being pushed back in your chair when you move forwards. Use this for rapid accelerations forward
    INPUTS:
    @@@@ intensity: value of 1, 2, or 3 for low, medium, and high intensity motion. This affects how quickly the platform moves and executes the effect

    ACCELERATE_FORWARDS sets values of data that are passed onto the Update function
    */
    public void ACCELERATE_FORWARDS(int intensity)
    {
        pholder = 100.0f; //try out just moving 7.5 degrees backwards from wherever the platform is. This value will most likely need to be tweaked in testing
        rholder = 0.0f;
        iholder = intensity; //should this be an int?
        readable = true;
        Debug.Log("works");
    }

    /*
    Braking is a function used to simulate the feeling of being flung out of your chair forward from rapid decceleration or backwards acceleration.
    INPUTS:
    @@@@ intensity: value of 1, 2, or 3 for low, medium, and high intensity motion. This affects how quickly the platform moves and executes the effect

    BRAKING sets values of data that are passed onto the Update function
    */
    public void BRAKING(int intensity)
    {
        pholder = 0.0f;
        rholder = 100.0f; //try out just moving 7.5 degrees forwards from wherever the platform is. This value will most likely need to be tweaked in testing
        iholder = intensity;
        readable = true;
    }

    /*
    Lean is a function used to simulate the leaning of a body left, right, or however you want it.
    INPUTS:
    @@@@ p: Float values between -100 and 100 that indicates the percentage the platforms pitch should change. -values tilt forward, +values tilt backwards 
    @@@@ r: Float values between -100 and 100 that indicates the percentage the platforms roll should change.  -values tilt left, +values tilt right

    $$description of what the function does$$
    */
    public void LEAN(float p, float r)
    {

    }

    /*
    Earthquake is an implementation of full_body_rumble that is meant to simulate an earthquake sensation. 
    INPUTS:
    @@@@ duration: Value in seconds that determines how long the effect will exist before it is stopped

    $$description of what the function does$$
    */
    public void EARTHQUAKE(float duration)
    {
        float intensity = 3; //max intensity is 3
        FULL_BODY_RUMBLE(intensity, duration);
    }

    private float motorAngleCalc(float q1, float q2, float[] Ap, float[] B)
    {

        //Constants

        float[] A_1 = { (float)(Ap[0] * Math.Cos(q2) + Ap[1] * Math.Sin(q1) * Math.Sin(q2) + Ap[2] * Math.Cos(q1) * Math.Sin(q2)), (float)(Ap[1] * Math.Cos(q1) + -Ap[2] * Math.Sin(q1)), (float)(-Ap[0] * Math.Sin(q2) + Ap[1] * Math.Sin(q1) * Math.Cos(q2) + Ap[2] * Math.Cos(q1) * Math.Cos(q2)) };
        float L_1 = 0;

        for (int i = 0; i < 3; i++)
        {
            L_1 += (float)Math.Pow((A_1[i] - B[i]), 2);
        }

        L_1 = (float)Math.Sqrt(L_1);
        Debug.Log("length is = " + L_1);
        if (L_1 > 12.53)
        {
            L_1 = 12.53f;
        }
        else if (L_1 < 9.67)
        {
            L_1 = 9.67f;
        }
        float r = 1.43f;
        float s = 11.1f;
        int sizeOfTheta = 5000;
        theta = new float[sizeOfTheta];
        L = new float[sizeOfTheta];

        float delta = (float)Math.PI / (sizeOfTheta - 1);

        for (int i = 0; i < sizeOfTheta; i++)
        {
            theta[i] = i * delta;
        }



        for (int i = 0; i < sizeOfTheta; i++)
        {
            float ct = (float)Math.Cos(theta[i]);
            float sSq = (float)Math.Pow(s, 2);
            float rSq = (float)Math.Pow(r, 2);
            float stSq = (float)Math.Pow(Math.Sin(theta[i]), 2);
            L[i] = (float)((r * ct) + Math.Sqrt((sSq - (rSq * stSq))));
        }

        //find where L and L_1 are equal

        int found = 0;
        int index1 = 0;
        float tempL = -1;
        float tempL1 = -1;

        while (found == 0)
        {
            if (index1 >= 5000)
            {
                found = 1;
            }
            else
            {
                tempL = (float)(Math.Round(L[index1] * 1000) / 1000);
                tempL1 = (float)(Math.Round(L_1 * 1000) / 1000);
                //Debug.Log(tempL + " " + tempL1);
                if ((tempL == tempL1) && ((tempL > -1) && (tempL1 > -1)))
                {
                    Debug.Log("yayyyyy");
                    Debug.Log(index1);
                    found = 1;
                }
                else
                {
                    index1++;
                }
            }
        }
        float thetas;
        if (index1 >= 5000)
        {
            float theta1 = 0;
            thetas = theta1;
        }
        else
        {
            float theta1 = theta[index1];
            thetas = theta1;
        }


        return thetas;

    }

}
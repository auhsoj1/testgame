﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnvironment : MonoBehaviour {

    //Objects
    GameObject currentLedge;
    Animator anim;

    //Public
    public bool nearLedge;

    //Private
    private Ray ray;
    private RaycastHit hit;
    private string colliderTag;

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        checkForLedge();
    }

    void checkForLedge()
    {
        ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 10) && nearLedge)
        {
            colliderTag = hit.collider.tag;
        }
        else
        {
            colliderTag = "";
        }
        if (colliderTag == "ledge")
        {
            if (Input.GetButtonDown("Action"))
            {
                anim.SetTrigger("Ledge");
                anim.applyRootMotion = true;
            }
        }
    }

    void Freeze()
    {
        //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GetComponent<PlayerController>().state = "hanging";
    }

    void Unfreeze()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<PlayerController>().state = "freeMove";
    }

    void LerpToLedge()
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, currentLedge.transform.localScale.y, 0), gameObject.GetComponent<Stats>().walkSpeed * Time.deltaTime);
        GetComponent<Rigidbody>().useGravity = false;
    }

    void LerpFromLedge()
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, 0, 6f), gameObject.GetComponent<Stats>().walkSpeed * Time.deltaTime);
        GetComponent<Rigidbody>().useGravity = false;
    }

    void ChangeCollider()
    {
        GetComponent<CapsuleCollider>().height = 0;
        GetComponent<CapsuleCollider>().center = new Vector3(0, 0, 0);
    }

    void ColliderEnable()
    {
        if (GetComponent<CapsuleCollider>().enabled)
        {
            GetComponent<CapsuleCollider>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
        }
        else
        {
            GetComponent<CapsuleCollider>().center = new Vector3(0, 1, 0);
            GetComponent<CapsuleCollider>().height = 2;
            GetComponent<CapsuleCollider>().enabled = true;
            GetComponent<Rigidbody>().useGravity = true;
            anim.applyRootMotion = false;
        }
    }

    void GravityEnable()
    {

    }

    void RootMotionEnable()
    {
        if (anim.applyRootMotion)
        {
            anim.applyRootMotion = false;
        }
        else
        {
            anim.applyRootMotion = true;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ledge")
        {
            currentLedge = other.gameObject;
            nearLedge = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "ledge")
        {
            nearLedge = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "FallTrigger")
        {
            anim.applyRootMotion = true;
            anim.SetTrigger("LedgeDrop");
        }
    }
}

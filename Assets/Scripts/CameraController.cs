﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    //Constants
    private const float Y_ANGLE_MIN = -60f;
    private const float Y_ANGLE_MAX = 60f;
    private const float MAX_ZOOM = 6.0f;
    private const float MIN_ZOOM = 2.0f;
    //Public variables

    //public Transform target;
    public float distance;
    public float sensitivityX;
    public float sensitivityY;
    //Private variables
    private Transform target;
    private float currentDistance = 0.0f;
    private float distToCol = Mathf.Infinity;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private Vector3 offset = new Vector3(0, 2.5f, 0);



    private void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
        currentDistance = distance;
        target = GameObject.Find("Player").transform;
    }

    private void Update()
    {
        float h = Input.GetAxisRaw("Horizontal1");
        float v = Input.GetAxisRaw("Vertical1");
        currentX += h * sensitivityX;
        currentY += v * sensitivityY;
        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
    }

    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -1 * currentDistance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        Vector3 targetPos = target.position + offset + rotation * dir;
        transform.position = targetPos;
        transform.LookAt(target.position + offset);
        if (toCamera())
        {
            currentDistance = distToCol - 0.2f;
        }
        currentDistance = Mathf.Lerp(currentDistance, distance, 0.1f);
    }

    private bool toCamera()
    {
        var layerMask = (1 << 9);
        layerMask |= (1 << 8);
        layerMask = ~layerMask;

        RaycastHit hit;
        bool collision = Physics.Raycast(target.position + offset, transform.position - (target.position + offset), out hit, distance, layerMask);
        if (collision)
        {
            distToCol = hit.distance;
        }
        else
        {
            distToCol = Mathf.Infinity;
        }
        return collision;
    }
}

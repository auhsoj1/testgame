﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    Animator anim;
    float speed;

    public Vector3 inputDir;
    public float throttle;

    //Lerps
    private float animSplit = 0f;       //Whether using the base layer, or the two split layers
    private float animSplitLerp = 0f;
    private float speedLerp;
    private float leftRightLerp = 0f;

    //Private
    private float speedMult = 1f;               //Speed multiplier
    private Vector3 groundNorm;                 //Normal of the ground which the player is standing on
    private float activeThrottle;          //Throttle to be used
    private float leftRight = 0f;


    new void Start () {

        base.Start();

        state = "freeMove";
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	protected new void Update () {

        base.Update();
        updateLerps();
        updateState();

        if (movementLag <= 0)
        {
            if (inputDir.magnitude > 0.05f)
            {
                moveDir = inputDir;

            }
            activeThrottle = throttle;
        }

        //Anim
        speed = Vector3.Dot(transform.forward, moveDir) * walkSpeed * activeThrottle * speedMult;
        leftRight = Vector3.Dot(transform.right, moveDir);
        anim.SetFloat("Speed", speedLerp / transform.lossyScale.y);
        anim.SetLayerWeight(0, animSplitLerp);
        anim.SetFloat("leftRight", leftRightLerp);
    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case "freeMove":
                speedMult = 1f;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveDir), Time.deltaTime * 10f);
                rigidBody.MovePosition(transform.position + Vector3.ProjectOnPlane(moveDir, groundNorm).normalized * Time.deltaTime * walkSpeed * activeThrottle * speedMult);
                break;

            case "hanging":
                break;
        }
    }

    void updateState()
    {

        switch (state)
        {
            case "freeMove":
                animSplit = 0f;
                anim.SetInteger("state", 0);
                break;

            case "hanging":
                animSplit = 0f;
                anim.SetInteger("state", 1);
                break;
        }
    }

    void updateLerps()
    {
        animSplitLerp = Mathf.Lerp(animSplitLerp, animSplit, Time.deltaTime * 8f);
        speedLerp = Mathf.Lerp(speedLerp, speed, Time.deltaTime * 5f);
        leftRightLerp = Mathf.Lerp(leftRightLerp, leftRight, Time.deltaTime * 7f);
    }
}

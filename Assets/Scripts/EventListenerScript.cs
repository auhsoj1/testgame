﻿using UnityEngine;
using UnityEngine.Events;

public class EventListenerScript : MonoBehaviour {

    public string lastMesgRecvd = "";
    public Material[] mats;

    private UnityAction<string> simpleEventListener;
    GameObject guard1;
    AudioSource audio;
    AudioClip[] clips;
    public bool b = false;
    public bool thrown = false;
    public bool resource = false;

    void Awake()
    {
        simpleEventListener = new UnityAction<string>(SomeFunction);
        guard1 = GameObject.Find("Guard1");
        audio = GetComponent<AudioSource>();
        clips = new AudioClip[] { (AudioClip)Resources.Load("footstep_concrete_1") as AudioClip, (AudioClip)Resources.Load("Slash 3") as AudioClip, (AudioClip)Resources.Load("Drop to Metal") as AudioClip};
    }

    void OnEnable()
    {
        EventManager.StartListening<SimpleEvent, string>(simpleEventListener);
    }

    void OnDisable()
    {
        EventManager.StopListening<SimpleEvent, string>(simpleEventListener);
    }

    void SomeFunction(string s)
    {
        lastMesgRecvd = s;
        
        if (b)
        {
            audio.PlayOneShot(clips[0]);
            b = false;
        }

        if (thrown)
        {
            audio.PlayOneShot(clips[1]);
            thrown = false;
        }

        if (resource)
        {
            audio.PlayOneShot(clips[2]);
            resource = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    //Public
    public string state;

    //Components
    protected Stats stats;
    protected Rigidbody rigidBody;
    protected new Collider collider;

    //Protected vars
    protected int actionLag;
    protected int movementLag;
    protected float walkSpeed;
    protected Vector3 moveDir = Vector3.forward;

    // Use this for initialization
    protected virtual void Start()
    {
        actionLag = 0;
        movementLag = 0;
        rigidBody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        stats = GetComponent<Stats>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        updateLag();
        updateStats();
    }

    private void updateLag()
    {
        if (movementLag > actionLag)
        {
            actionLag = movementLag;
        }
        if (actionLag > 0)
        {
            actionLag--;
        }
        if (movementLag > 0)
        {
            movementLag--;
        }
    }

    public void addMovementLag(int amount)
    {
        movementLag += amount;
    }

    public void addActionLag(int amount)
    {
        actionLag += amount;
    }

    public void setMoveDir(Vector3 dir)
    {
        moveDir = dir.normalized;
    }

    private void updateStats()
    {
        walkSpeed = stats.walkSpeed;
    }
}

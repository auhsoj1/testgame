﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    GameObject player;
    Animator anim;
    PlayerController controller;
	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        anim = player.GetComponent<Animator>();
        controller = player.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {

        float horiz = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");

        anim.SetFloat("Horizontal", horiz);
        anim.SetFloat("Vertical", vert);
        Vector3 forward = transform.forward;
        Vector3 right = transform.right;
        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();
        Vector3 movement = (forward * vert + right * horiz);

        if (movement.magnitude < 1f)
        {
            controller.throttle = movement.magnitude;
        }
        else
        {
            controller.throttle = 1f;
        }
        //if (player.GetComponent<PlayerController>().state != "hanging")
        //{
            controller.inputDir = movement.normalized;
        //}
    }
}

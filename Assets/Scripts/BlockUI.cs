﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockUI : MonoBehaviour {

    GameObject block;
    GameObject player;
    GameObject canvas2;
	// Use this for initialization
	void Start () {
        block = GameObject.Find("Block");
        player = GameObject.Find("Player");
        canvas2 = GameObject.Find("Canvas2");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject == player)
        {
            canvas2.SetActive(true);
        }
    }
}

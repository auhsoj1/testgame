﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;
using System.Threading;
using System.IO;

public class BluetoothScript : MonoBehaviour
{

    private SerialPort serialPort = new SerialPort("COM7", 115200, Parity.None, 8, StopBits.One);
    int readTimeOut = 1000;
    int writeTimeOut = 1000;
    int bufferSize = 32;                // Device sends 32 bytes per packet

    byte[] buffer = { 1, 2 };

    void Start()
    {
        serialPort.ReadTimeout = readTimeOut;
        serialPort.WriteTimeout = writeTimeOut;

        /*try
        {
            serialPort.Open();
        }
        catch (IOException ex)
        {
            Debug.Log(ex);
        }*/

        //if (!serialPort.IsOpen)
            serialPort.Open();
        //if (serialPort.IsOpen)
        //{
          //  serialPort.WriteLine("hello world");
            //serialPort.Close();
        //}

    }



    void Update()
    {
        //Debug.Log(serialPort.ReadLine());
        serialPort.WriteLine("3");
    }

    void SendValues()
    {
        serialPort.Write("hello" + "\n");
        serialPort.Write("world" + "\n");
    }

    void waitForUserInput()
    {
        Byte[] buffer = new Byte[bufferSize];
        Byte a = 1;
        Byte b = 3;
        Byte c = 15;
        buffer[0] = a;
        buffer[1] = b;
        buffer[2] = c;
        Debug.Log(buffer[0]);
        try
        {
            //serialPort.Write(buffer, 0, 3);
            //serialPort.Write(msg);
        }
        catch (TimeoutException e)
        {
            Debug.Log(e.Message);
        }
        
        //Debug.Log(serialPort.ReadLine());
    }
}